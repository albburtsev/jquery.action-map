(function($) {
	'use strict';

	var classPositive = 'plus',
		classNegative = 'minus',
		isSVG;

	// SVG detection
	try {
		isSVG = document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#BasicStructure', '1.1');
	} catch(e) {}

	function createSVG(tag, attrs) {
		var el = document.createElementNS('http://www.w3.org/2000/svg', tag);
		for (var attr in attrs) {
			if ( attrs.hasOwnProperty(attr) ) {
				el.setAttribute(attr, attrs[attr]);
			}
		}
		return el;
	}

	function createVML(tag, attr) {
		var el = document.createElementNS('http://www.w3.org/2000/svg', tag, attr || {});
		return el;
	}

	$.fn.actionMap = function(opts, callback) {
		return $.each(this, function() {
			var	_this = $(this),
				_image = $('.action-map__image', this),
				_layer = $('.action-map__layer', this),
				_map = $('.action-map__areas', this),
				_areas = $('area', _map),
				_polygons,
				_canvas;

			var	canvasWidth = _image.width(),
				canvasHeight = _image.height();

			opts = $.extend({
				duration: 200, // ms
				stroke: '#FFF',
				fill: '#FFF',
				'stroke-width': '2px',
				'fill-opacity': 0.5
			}, opts || {});

			callback = callback || function() {};

			function createCanvas() {
				if ( isSVG ) {
					_canvas = $(createSVG('svg', {
						width: canvasWidth,
						height: canvasHeight,
						viewPort: '0 0 ' + canvasWidth + ' ' + canvasHeight
					})).appendTo(_layer);
				} else {
					_canvas = $(createVML('svg'))
						.css({
							width: canvasWidth + 'px',
							height: canvasHeight + 'px'
						})
						.appendTo(_layer);
				}
			}

			function createPolygon(points) {
				var _poly, poly;

				if ( isSVG ) {
					_poly = $(createSVG('polygon', $.extend({ points: points }, opts)))
						.appendTo(_canvas)
						.css('opacity', 0);
				} else {
					poly = createVML('polygon', {
						width: canvasWidth,
						height: canvasHeight
					});

					poly.setAttribute('points', points);
					poly.setAttribute('stroke', opts.stroke);
					poly.setAttribute('stroke-width', opts['stroke-width']);
					poly.setAttribute('fill', opts.fill);
					poly.setAttribute('fill-opacity', 0.5);

					_poly = $(poly).appendTo(_canvas);
					_poly.css('display', 'none');
				}

				return _poly;
			}
		
			function setLabelValue(_el, value) {
				_el
					.removeClass(classPositive)
					.removeClass(classNegative)
					.addClass(value ? (value > 0 ? classPositive : classNegative) : '')
					.find('.js-value')
					.html(value || '');
			}

			// Sets canvas layer size
			_layer.css({
				width: canvasWidth,
				height: canvasHeight
			});

			// Creates canvas
			createCanvas();

			// Adds polygons for all areas
			_areas.each(function() {
				var _area = $(this),
					_poly = createPolygon(_area.attr('coords')),
					rel = _area.attr('rel'),
					isShape = _poly.is('shape'),
					fixed = false;

				if ( isShape ) {
					_poly.get(0).rel = rel;
				} else {
					_poly.attr('rel', rel);
				}

				_area
					.on('mouseenter', function() {
						if ( isShape ) {
							_polygons.each(function() {
								if ( this.rel === rel ) {
									this.style.display = '';
								}
							});
						} else {
							_polygons
								.filter('[rel="' + rel + '"]')
								.stop()
								.animate({ opacity: 1 }, opts.duration);
						}
					})
					.on('mouseleave', function() {
						if ( isShape ) {
							_polygons.each(function() {
								if ( this.rel === rel ) {
									this.style.display = 'none';
								}
							});
						} else {
							_polygons
								.filter('[rel="' + rel + '"]')
								.stop()
								.animate({ opacity: 0 }, opts.duration);
						}
					});
			});

			// Gets polygons
			if ( isSVG ) {
				_polygons = $('polygon', _canvas);
			} else {
				_polygons = $('shape', _canvas);
			}

			// Callback invocation
			callback.call(this, _polygons);
		});
	};

	$(function() {
		$('.action-map').each(function() {
			var _this = $(this),
				data = _this.data();

			if ( data.autoinit !== 'off' ) {
				_this.actionMap();
			}
		});
	});
})(jQuery);
