/**
 * SVG to VML patch, partial (only polygons) SVG support for IE6-8
 */
(function() {
	if ( document.implementation.hasFeature('http://www.w3.org/TR/SVG11/feature#BasicStructure', '1.1')	) {
		return;
	}

	document.namespaces.add('v', 'urn:schemas-microsoft-com:vml');

	var	styleSheet = document.createStyleSheet(),
		vmlSelectors = [' *', 'roundrect', 'oval', 'fill', 'line', 'shape', 'polyline', 'stroke'],
		vmlValue = 'behavior:url(#default#VML); position:absolute';

	for (var i = 0; i < vmlSelectors.length; i++) {
		styleSheet.addRule('v\\:' + vmlSelectors[i], vmlValue);
	}

	var	attributes = {};

		/** @ignore */
		attributes.stroke = function(element, value) {
			element.stroked = value == 'none' ? 0 : 1;
			element.strokecolor = value;
		};
		
		/** @ignore */
		attributes.fill = function(element, value) {
			element.filled = value == 'none' ? 0 : 1;
			element.fillcolor = value ;
		};

		/** @ignore */
		attributes['stroke-width'] = function(element, value) {
			element.strokeweight = parseFloat(value) / 1.2 + 'pt';
		};
		
		/** @ignore */
		attributes['stroke-opacity'] = function(element, value) {
			var stroke = document.createElement('v:stroke');
			stroke.endcap = 'round';
			stroke.opacity = value;
			element.appendChild(stroke);
		};
		
		/** @ignore */
		attributes['fill-opacity'] = function(element, value) {
			var fill = document.createElement('v:fill');
			fill.opacity = value;
			element.appendChild(fill);
		};

		/** @ignore */
		attributes.d = function(element, value) {
			element.path = value;
		};

	document.createElementNS = function(ns, name, attr) {
		var	element, child,
			props = { x: 'left', y: 'top' },
			svg2vmlStyle = {
				fill: { value: '#000000', valueSource: 'default' },
				stroke: { value: 'none', valueSource: 'default' },
				'stroke-width': { value: '1', valueSource: 'default' }
			};

		switch (name) {
			case 'svg':
				element = document.createElement('div');
				element.setAttribute('baseInit', false);
				element.setAttribute('baseX', 0);
				element.setAttribute('baseY', 0);
			break;

			case 'polygon' :
				element = document.createElement('v:shape');
				element.endcap = 'square';
				element.coordsize = attr.width + ', ' + attr.height;
				element.style.width = attr.width + 'px';
				element.style.height = attr.height + 'px';

				element.svg2vmlStyle = svg2vmlStyle;

				/** @ignore */
				element.setAttribute = function(key, value, cascade) {
					if (this.svg2vmlStyle[key]){
						this.svg2vmlStyle[key].value = value;
						this.svg2vmlStyle[key].valueSource = (cascade || 'user');
					}

					if ( key == 'points' ) {
						var source = value.split(','),
							points = '';
						
						points += 'm' + source.shift();
						points += ',' + source.shift();
						points += 'l' + source.join(',');

						this.path = points;
					} else if ( attributes[key] ) {
						attributes[key](this, value);
					}
				};

				return element;
		}

		if ( name != 'svg' ) {
			for (var key in element.svg2vmlStyle) {
				if (
					element.svg2vmlStyle[key].value &&
					element.svg2vmlStyle[key].valueSource == 'default'
				) {
					element.setAttribute(key, element.svg2vmlStyle[key].value, 'default');
				}
			}
		}

		return element;
	};
})();
